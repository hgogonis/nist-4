# Technologies

* Apache Spark
* spark-csv
* OSRM (Open Source Routing Machine)

# Techniques

We use spark to allow for parallized tabular data manipulation
and for map reduce functionallity

We first pre-prossessed the given training CSV file by using the 
OSRM API to map each event to a road segment. 
We also had to "clean" the data by stripping newlines that occured in the middle of fields of the CSV file.

Then using spark, for each input line we:

    * Filter the training data so that it fits in bounding box
    * Group by road segment and event type
    * Aggregate the count of events for each group
    * *At this stage we would like to use MLLib for regression*
    * Aggregate the number of events for each type and normalize
    * Print estimated number of events for the input to the file
