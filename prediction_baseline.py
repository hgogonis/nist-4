# Baseline approach for prediction.

import csv
import os
import sys
from tqdm import *
import requests
import pandas as pd

from pyspark import SparkContext, SparkConf

csv.field_size_limit(sys.maxsize)

def getNearestNode(lon,lat):
    uri = 'http://localhost:5000/nearest?loc=%f,%f' % (lat,lon)
    r = requests.get(uri)
    return r.json()

def cleanTrain():
    if os.path.isfile('events_train_clean.csv'): return
    
    print "Generating new CSV file by lookuping up nearest street segment for each event"
    with open('events_train.csv', 'rb') as csvfile:
        csvFile = csv.reader(csvfile, delimiter=',')
        header = csvFile.next()
        header.append('nearest_lat')
        header.append('nearest_lon')
        rows = [r for r in csvFile if r[10].find('.') > -1]
    
    for r in tqdm(rows):  
        lon = float(r[10])
        lat = float(r[9])
        # {u'status': 0, u'name': u'', u'mapped_coordinate': [38.928738, -77.241829]}
        mapped_lat, mapped_lon = getNearestNode(lon,lat)['mapped_coordinate']
        r.append(mapped_lat)
        r.append(mapped_lon)
    
    writer = csv.writer(open('events_train_clean.csv', 'w'))
    writer.writerow(header)
    writer.writerows(rows)

if __name__=="__main__":

    # verify files
    assert os.path.isfile('events_train.csv'), "Error: you have to place the events_train.csv file to the script folder."
    assert os.path.isfile('prediction_trials.tsv'), "Error: you have to place the prediction_trials.tsv file to the script folder."
    
    cleanTrain()
    
    '''
    # read testing cases.
    with open('prediction_trials.tsv','rb') as csvfile:
        tc = [(float(r[1]),float(r[0]),float(r[3]),float(r[2])) for r in csv.reader(csvfile, delimiter='\t')]	# [(xmin,xmax,ymin,ymax)]
    '''
    #load training data.
    myLines = sc.textFile('events_train_clean.csv')
    #with open('events_train.csv', 'rb') as csvfile:
    #    rows = [r for r in csv.reader(csvfile, delimiter=',') if r[10].find('.') > -1]
'''	
    # predict the first 10 testing cases, based on the counts of events in 2014.
    for xmin,xmax,ymin,ymax in tc[0:1]:
        bounds = []
        for r in rows:
            lon = float(r[10])
            lat = float(r[9])
            if r[4].split('-')[0] == '2014' and lon>xmin and lon<xmax and lat>ymin and lat<ymax:
                bounds.append(r)

        print bounds[0]
      #print "%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f" % (cnt['accidentsAndIncidents']/12.0,cnt['roadwork']/12.0,cnt['precipitation']/12.0,cnt['deviceStatus']/12.0,cnt['obstruction']/12.0,cnt['trafficConditions']/12.0)
'''
