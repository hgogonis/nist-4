import pandas as pd
import requests
from xml.etree import ElementTree

def getOSMData(bb):
  ''' Returns all 'way' (e.g, highway)  associated with a given bounding box
      as a Pandas DataFrame
  '''

  # Send API request
  uri = ("http://www.openstreetmap.org/api/0.6/map?bbox=%f,%f,%f,%f" % bb)
  print 'Requesting data from openstreetmap.org'
  r = requests.get(uri)
  print r.headers
  if 'erorr' in r.headers:
    print r.headers['error']
    return 'Error loading data'

  # Parse XML - Extract 'Way'
  root = ElementTree.fromstring(r.content)
  data = []
  for child in root:
    if child.tag == 'way': data.append(child.attrib)
  
  # Create a DataFrame
  print 'generating DataFrame'
  data_frame = pd.DataFrame(data)
  return data_frame

def getBoundingBox(shape):
  lngs = [coord[0] for coord in shape]
  lats = [coord[1] for coord in shape]
  return min(lngs), min(lats), max(lngs), max(lats)

area1 = [
            [
              -77.06801891326904,
              38.890598588891805
            ],
            [
              -77.06801891326904,
              38.894573433974436
            ],
            [
              -77.05231189727783,
              38.894573433974436
            ],
            [
              -77.05231189727783,
              38.890598588891805
            ],
            [
              -77.06801891326904,
              38.890598588891805
            ]
          ]
bb = getBoundingBox(area1)
print getOSMData(bb)
