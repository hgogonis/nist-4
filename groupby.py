import os
import sys
import csv

if os.path.exists('prediction_submissions_12.txt'):
    print "Output file already exists"
    sys.exit(1)

if not os.path.exists('prediction_trials.tsv'):
    print "you need to have the prediction_trails.tsv file in this folder"
    sys.exit(1)

if not os.path.exists('events_train_clean.csv'):
    print "you need to have the events_train_clean file in this folder"
    sys.exit(1)

pyspark_submit_args = os.environ.get('PYSPARK_SUBMIT_ARGS', '')
if not "pyspark-shell" in pyspark_submit_args: pyspark_submit_args += " pyspark-shell"
os.environ['PYSPARK_SUBMIT_ARGS'] = pyspark_submit_args

spark_home = os.environ.get('SPARK_HOME', None)
if not spark_home:
    raise ValueError('SPARK_HOME environment variable is not set')
sys.path.insert(0, os.path.join(spark_home, 'python'))
#sys.path.insert(0, os.path.join(spark_home, 'python/lib/py4j-0.8.1-src.zip'))
execfile(os.path.join(spark_home, 'python/pyspark/shell.py'))

from pyspark.sql import SQLContext, Row
from pyspark.sql.functions import *
sqlContext = SQLContext(sc)

# 675234 Total lines

with open('prediction_trials.tsv','rb') as csvfile:
    tc = [(float(r[1]),float(r[0]),float(r[3]),float(r[2]),r[4]) for r in csv.reader(csvfile, delimiter='\t')]

df = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true', mode='DROPMALFORMED').load('events_train_clean.csv')

i=0
lines = []
for xmin,xmax,ymin,ymax,date_min in tc:
    i = i + 1
    date_split = date_min.split('-')
    date_year = date_split[0]
    date_month = date_split[1]
	#date_min, date_max = [to_date(lit(s)).cast(TimestampType()) for s in dates]

    f = df.filter("longitude > %s AND longitude < %s AND latitude > %s AND latitude < %s" % (xmin,xmax,ymin,ymax) )
    filtered_date = f.filter( (month(f.start_tstamp) == date_month) ) # & (year(f.start_tstamp) == date_year) )
    eventTypes = ['accidentsAndIncidents','obstruction', 'trafficConditions',
                    'deviceStatus', 'roadwork', 'precipitation' ]
    filtered = filtered_date.filter(filtered_date.event_type.isin(eventTypes))
    grouped = filtered.groupBy('segment_latitude', 'segment_longitude', 'event_type').agg(count('*').alias('c')).select('c', 'event_type')

    insert = [ {'event_type':'accidentsAndIncidents', 'c':0},
               {'event_type':'roadwork', 'c':0},
               {'event_type':'precipitation', 'c':0},
               {'event_type':'deviceStatus', 'c':0},
               {'event_type':'obstruction', 'c':0},
               {'event_type':'trafficConditions', 'c':0}]

    temp = sqlContext.createDataFrame(insert)
    grouped = temp.unionAll(grouped)
    mapped = grouped.map(lambda x: ( str(x.event_type), x.count(1) )).reduceByKey(lambda a,b: (a+b))
    
    n = 8
    line = "%s\t%s\t%s\t%s\t%s\t%s\n" % (
            mapped.lookup('accidentsAndIncidents')[0]/n+1 or 0,
            mapped.lookup('roadwork')[0]/n+1 or 0,
            mapped.lookup('precipitation')[0]/n+1 or 0,
            mapped.lookup('deviceStatus')[0]/n+1 or 0,
            mapped.lookup('obstruction')[0]/n+1 or 0,
            mapped.lookup('trafficConditions')[0]/n+1 or 0)
    with open('prediction_submissions.txt', 'a') as outputFile:
        outputFile.write(line)
