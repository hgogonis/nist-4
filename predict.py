#!/usr/bin/env python
import os
import sys
import csv


from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, Row
from pyspark.sql.functions import *

def predict(xmin, xmax, ymin, ymax, date_min, df):
    date_month = date_min.split('-')[1]

    f = df.filter("longitude > %s AND longitude < %s AND latitude > %s AND latitude < %s" % (xmin,xmax,ymin,ymax) )
    filtered_date = f.filter( (month(f.start_tstamp) == date_month) ) # & (year(f.start_tstamp) == date_year) )
    eventTypes = ['accidentsAndIncidents','obstruction', 'trafficConditions',
                    'deviceStatus', 'roadwork', 'precipitation' ]
    filtered = filtered_date.filter(filtered_date.event_type.isin(eventTypes))
    grouped = filtered.groupBy('segment_latitude', 'segment_longitude', 'event_type').agg(count('*').alias('c')).select('c', 'event_type')

    insert = [ {'event_type':'accidentsAndIncidents', 'c':0},
               {'event_type':'roadwork', 'c':0},
               {'event_type':'precipitation', 'c':0},
               {'event_type':'deviceStatus', 'c':0},
               {'event_type':'obstruction', 'c':0},
               {'event_type':'trafficConditions', 'c':0}]

    temp = sqlContext.createDataFrame(insert)
    grouped = temp.unionAll(grouped)
    mapped = grouped.map(lambda x: ( str(x.event_type), x.count(1) )).reduceByKey(lambda a,b: (a+b))
    
    n = 9
    line = "%s\t%s\t%s\t%s\t%s\t%s\n" % (
            mapped.lookup('accidentsAndIncidents')[0]/n+1 or 0,
            mapped.lookup('roadwork')[0]/n+1 or 0,
            mapped.lookup('precipitation')[0]/n+1 or 0,
            mapped.lookup('deviceStatus')[0]/n+1 or 0,
            mapped.lookup('obstruction')[0]/n+1 or 0,
            mapped.lookup('trafficConditions')[0]/n+1 or 0)
    return line

if __name__ == "__main__":

    sc = SparkContext(appName="NISTpredict")
    sqlContext = SQLContext(sc)

    # 675234 Total lines

    data = sc.textFile('prediction_trials.tsv').map(lambda x: x.split('\t')).collect()
    df = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true', mode='DROPMALFORMED').load('events_train_clean.csv')

    df.cache()

    for r in data:
        output = predict(float(r[1]),float(r[0]),float(r[3]),float(r[2]),r[4], df)
        with open('prediction_submissions.txt', 'a') as outputFile:
            outputFile.write(output)
